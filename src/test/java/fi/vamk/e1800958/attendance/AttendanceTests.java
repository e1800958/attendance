package fi.vamk.e1800958.attendance;

import static org.assertj.core.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.collections4.IterableUtils;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.junit4.SpringRunner;

import fi.vamk.e1800958.attendance.Attendance;
import fi.vamk.e1800958.attendance.AttendanceRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
@Configuration
@ComponentScan(basePackages = { "fi.vamk.tka.semdemo" })
@EnableJpaRepositories(basePackageClasses = AttendanceRepository.class)
public class AttendanceTests {
    @Autowired
    private AttendanceRepository repository;

    /*
     * Testcase 1 The solution can save, fetch by key and delete the attendance
     *
     */
    @Test
    public void postGetDeleteAtteandance() {
        Iterable<Attendance> begin = repository.findAll();
        // System.out.println(IterableUtils.size(begin));
        // given
        Attendance att = new Attendance("ABCD");
        System.out.println("ATT: " + att.toString());
        // test save
        Attendance saved = repository.save(att);
        System.out.println("SAVED: " + saved.toString());
        // when
        Attendance found = repository.findByKey(att.getKey());
        System.out.println("FOUND " + found.toString());
        // then
        assertThat(found.getKey()).isEqualTo(att.getKey());
        repository.delete(found);
        Iterable<Attendance> end = repository.findAll();
        // System.out.println(IterableUtils.size(end));
        assertEquals((long) IterableUtils.size(begin), (long) IterableUtils.size(end));
    }
    
    @Test
    public void testFindByDate() {
    	
    	

		try {
			
			Date date = new SimpleDateFormat("dd-MM-yyyy").parse("03-03-2010");
			Attendance att= new Attendance(1, "ab", date);
			Attendance saved = repository.save(att);
			AttendanceDTO attDTO= repository.findByDay(date).convert();
			
	        assertThat(att.convert().toString()).isEqualTo(attDTO.toString());
		} catch (ParseException e) {
			System.out.println("Invalid date");
		}
        
        
    }
}
