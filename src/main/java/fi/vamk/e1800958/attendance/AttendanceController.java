package fi.vamk.e1800958.attendance;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


import org.springframework.web.bind.annotation.ResponseBody;

@RestController
public class AttendanceController {
    @Autowired
    private AttendanceRepository repository;

    @GetMapping("/attendances")
    public Iterable<AttendanceDTO> list() {
    	ArrayList< AttendanceDTO> list= new ArrayList<AttendanceDTO>();
    	for (Attendance a : repository.findAll()) {
			list.add(a.convert());
		}
        return list;
    }
    
    
    @GetMapping("/attendance-date/{date}")
    public AttendanceDTO getByDate(@PathVariable("date") String date) {
		try {			
			Date date1 = new SimpleDateFormat("dd-MM-yyyy").parse(date);
			return repository.findByDay(date1).convert();
		} catch (ParseException e) {
			return null;
		}
    }
    
    @GetMapping("/attendance-id/{id}")
    public AttendanceDTO getById(@PathVariable("id") int id) {
		return repository.findById(id).get().convert();
    }

    @PostMapping("/attendance")
    public @ResponseBody AttendanceDTO create(@RequestBody AttendanceDTO item) {
    	Attendance a= item.convert();
    	if (a.getDay() == null) return null;
        return repository.save(a).convert();
    }

    @PutMapping("/attendance")
    public @ResponseBody AttendanceDTO update(@RequestBody AttendanceDTO item) {
    	Attendance a= item.convert();
    	if (a.getDay() == null) return null;
        return repository.save(a).convert();
    }

    @DeleteMapping("/attendance")
    public void date(@RequestBody AttendanceDTO item) {
    	Attendance a= item.convert();
//    	if (a.getDay() != null) repository.delete(a);
    	repository.delete(a);
    }
}