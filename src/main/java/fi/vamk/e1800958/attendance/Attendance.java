package fi.vamk.e1800958.attendance;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.*;

@Entity
@NamedQuery(name = "Attendance.findAll", query = "SELECT p FROM Attendance p")
public class Attendance implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String key;
	private Date day;

	public Date getDay() {
		return this.day;
	}

	public void setDay(Date day) {
		this.day = day;
	}

	public Attendance(String key) {
		this.key = key;
	}

	public Attendance() {
	}
	
	

	public Attendance(int id, String key, Date day) {
		super();
		this.id = id;
		this.key = key;
		this.day = day;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getKey() {
		return this.key;
	}

	public void setKey(String key) {
		this.key = key;
	}
	
	
	public AttendanceDTO convert()
	{	
			SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
			String strDate = formatter.format(this.getDay());  
			return new AttendanceDTO(this.getId(), this.getKey(), strDate);	
		
	}
	

	public String toString() {
		return id + " " + key + " " + day;
	}
}